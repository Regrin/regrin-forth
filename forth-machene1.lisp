(defvar *data-stack* nil)

(defparameter *dictionary*
  (make-forth-dictionary
      (list '+ #'(lambda (stack)
                     (let ((a (pop stack))
                           (b (pop stack)))
                         (cons (+ a b)
                               stack))))
      
      (list '- #'(lambda (stack)
                     (let ((a (pop stack))
                           (b (pop stack)))
                         (cons (- a b)
                               stack))))
      
      (list '* #'(lambda (stack)
                     (let ((a (pop stack))
                           (b (pop stack)))
                         (cons (* a b)
                               stack))))
      
      (list '/ #'(lambda (stack)
                     (let ((a (pop stack))
                           (b (pop stack)))
                         (cons (/ a b)
                               stack))))
      
      (list '\. #'(lambda (stack)
                     (print (car stack))
                     (cdr stack)))
      
      (list 'dup #'(lambda (stack)
                       (cons (car stack)
                             stack)))
      (list 'swap #'(lambda (stack)
                        (let ((a (pop stack))
                              (b (pop stack)))
                            (push a stack)
                            (push b stack))))))

(defun make-forth-dictionary (&rest words)
    (loop for word in words
               collect (make-forth-word :name (first word)
                                        :definition (second word))))

(defstruct forth-word
  name
  definition)

(defmacro aif (test then &optional else)
    `(let ((it ,test))
         (if it
             ,then
             ,else)))

(defun find-word-in-dictionary (word-symbol dictionary)
    (loop for w in dictionary
          do (when (equal word-symbol
                          (forth-word-name w))
                 (return w))))

(defun find-word-definition-in-dictionary (word-symbol dictionary)
    (aif (find-word-in-dictionary word-symbol dictionary)
         (forth-word-definition it)))


(defun eval-forth-word (word-symbol stack dictionary)
    (block nil
        (aif (find-word-definition-in-dictionary word-symbol dictionary)
             (progn (print it)
                    (when (functionp it)
                        (return (funcall it stack)))
                    (when (listp it)
                        (dolist (word it)
                            (setf stack (eval-forth-word word stack dictionary)))
                        stack))
             (format t "Ошибка. Неизвестное слово"))))
    
